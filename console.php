<?php

require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;
use UEFA\Console\GameCommand;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use \Symfony\Component\Config\FileLocator;

try {
    $container = new ContainerBuilder();
    $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/src/Config'));
    $loader->load('services.yml');

    $application = new Application();
    $application->add(new GameCommand($container->get('game_manager.service')));

    $application->run();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
