<?php

namespace UEFA\Tests;

use PHPUnit\Framework\TestCase;
use UEFA\Core\DataSource\Yaml;
use UEFA\Core\Mapper\PlayerMapper;
use UEFA\Entity\OpponentsDifficulty;
use UEFA\Entity\Player;
use UEFA\Repository\PlayerRepository;
use UEFA\Service\Team\TeamManagerImpl;
use Exception;
use UEFA\ValueObject\Position;

class TeamManagerTest extends TestCase
{
    private PlayerRepository $playerRepository;

    protected function setUp(): void
    {
        $dataSource = new Yaml(new PlayerMapper());

        $availablePlayers = $dataSource->collectDataFromSource()->getData();

        $this->playerRepository = $this->createMock(PlayerRepository::class);
        $this->playerRepository->method('all')->willReturn($availablePlayers);
    }

    /**
     * @throws Exception
     */
    public function testAssembleTeamForStrongOpponent(): void
    {
        $teamManager = new TeamManagerImpl($this->playerRepository);
        $team = $teamManager->selectTeamByDifficulty(OpponentsDifficulty::DIFFICULTY_STRONG);
        $defenders = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::DEFENDER;
        });

        $midfielders = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::MIDFIELDER;
        });

        $attackers = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::ATTACKER;
        });

        $goalKeeper = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::GOAL_KEEPER;
        });

        self::assertCount(11, $team);
        self::assertCount(5, $defenders);
        self::assertCount(4, $midfielders);
        self::assertCount(1, $attackers);
        self::assertCount(1, $goalKeeper);
    }

    /**
     * @throws Exception
     */
    public function testGradesTeamForStrongOpponent(): void
    {
        $teamManager = new TeamManagerImpl($this->playerRepository);
        $team = $teamManager->selectTeamByDifficulty(OpponentsDifficulty::DIFFICULTY_STRONG);
        $defenders = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::DEFENDER;
        });

        $midfielders = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::MIDFIELDER;
        });

        $attackers = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::ATTACKER;
        });

        $goalKeeper = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::GOAL_KEEPER;
        });

        self::assertEquals(37, $this->calculateGradeSum($defenders));
        self::assertEquals(34, $this->calculateGradeSum($midfielders));
        self::assertEquals(8, $this->calculateGradeSum($goalKeeper));
        self::assertEquals(9, $this->calculateGradeSum($attackers, true));
    }

    private function calculateGradeSum(array $players, $sumSpeed = false): int
    {
        return array_reduce($players, static function ($sum, Player $player) use ($sumSpeed) {
            $sum += $sumSpeed ?
                $player->getSpeed()->getValue() :
                $player->getQuality()->getValue();

            return $sum;
        });
    }

    /**
     * @throws Exception
     */
    public function testAssembleTeamForEqualOpponent(): void
    {
        $teamManager = new TeamManagerImpl($this->playerRepository);
        $team = $teamManager->selectTeamByDifficulty(OpponentsDifficulty::DIFFICULTY_EQUAL);
        $defenders = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::DEFENDER;
        });

        $midfielders = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::MIDFIELDER;
        });

        $attackers = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::ATTACKER;
        });

        $goalKeeper = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::GOAL_KEEPER;
        });

        self::assertCount(11, $team);
        self::assertCount(4, $defenders);
        self::assertCount(4, $midfielders);
        self::assertCount(2, $attackers);
        self::assertCount(1, $goalKeeper);
    }

    /**
     * @throws Exception
     */
    public function testGradesTeamForEqualOpponent(): void
    {
        $teamManager = new TeamManagerImpl($this->playerRepository);
        $team = $teamManager->selectTeamByDifficulty(OpponentsDifficulty::DIFFICULTY_EQUAL);
        $defenders = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::DEFENDER;
        });

        $midfielders = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::MIDFIELDER;
        });

        $attackers = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::ATTACKER;
        });

        $goalKeeper = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::GOAL_KEEPER;
        });

        self::assertEquals(32, $this->calculateGradeSum($defenders));
        self::assertEquals(34, $this->calculateGradeSum($midfielders));
        self::assertEquals(8, $this->calculateGradeSum($goalKeeper));
        self::assertEquals(20, $this->calculateGradeSum($attackers));
    }

    /**
     * @throws Exception
     */
    public function testAssembleTeamForWeakOpponent(): void
    {
        $teamManager = new TeamManagerImpl($this->playerRepository);
        $team = $teamManager->selectTeamByDifficulty(OpponentsDifficulty::DIFFICULTY_WEAK);
        $defenders = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::DEFENDER;
        });

        $midfielders = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::MIDFIELDER;
        });

        $attackers = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::ATTACKER;
        });

        $goalKeeper = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::GOAL_KEEPER;
        });

        self::assertCount(11, $team);
        self::assertCount(3, $defenders);
        self::assertCount(4, $midfielders);
        self::assertCount(3, $attackers);
        self::assertCount(1, $goalKeeper);
    }

    /**
     * @throws Exception
     */
    public function testGradesTeamForWeakOpponent(): void
    {
        $teamManager = new TeamManagerImpl($this->playerRepository);
        $team = $teamManager->selectTeamByDifficulty(OpponentsDifficulty::DIFFICULTY_WEAK);
        $defenders = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::DEFENDER;
        });

        $midfielders = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::MIDFIELDER;
        });

        $attackers = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::ATTACKER;
        });

        $goalKeeper = array_filter($team, static function(Player $player) {
            return $player->getPosition()->getValue() === Position::GOAL_KEEPER;
        });

        self::assertEquals(26, $this->calculateGradeSum($defenders));
        self::assertEquals(34, $this->calculateGradeSum($midfielders));
        self::assertEquals(8, $this->calculateGradeSum($goalKeeper));
        self::assertEquals(27, $this->calculateGradeSum($attackers));
    }

    /**
     * @throws Exception
     */
    public function testInjuriesHandling(): void
    {
        $teamManager = new TeamManagerImpl($this->playerRepository);
        $team = $teamManager->selectTeamByDifficulty(OpponentsDifficulty::DIFFICULTY_STRONG);
        /** @var Player $injuredPlayer */
        $injuredPlayer = $team[random_int(0, 10)];
        $teamManager->injury($injuredPlayer);

        $teamForNextMatch = $teamManager->selectTeamByDifficulty(OpponentsDifficulty::DIFFICULTY_EQUAL);

        $injuredPlayerInNewTeamCount = array_filter($teamForNextMatch, static function(Player $player) use ($injuredPlayer) {
            return $player->getName() === $injuredPlayer->getName();
        });

        self::assertCount(0, $injuredPlayerInNewTeamCount);
    }
}
