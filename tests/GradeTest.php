<?php

namespace UEFA\Tests;

use PHPUnit\Framework\TestCase;
use UEFA\ValueObject\Grade;
use InvalidArgumentException;

class GradeTest extends TestCase
{
    public function testCreateValidGrade(): void
    {
        $grade = new Grade(5);

        self::assertEquals(5, $grade->getValue());
    }

    public function testCreateToHighGrade(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new Grade(11);
    }

    public function testCreateToLowGrade(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new Grade(0);
    }
}
