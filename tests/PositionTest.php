<?php

namespace UEFA\Tests;

use PHPUnit\Framework\TestCase;
use UEFA\ValueObject\Position;
use InvalidArgumentException;

class PositionTest extends TestCase
{
    public function testCreateDefender(): void
    {
        $defenderPosition = new Position(Position::DEFENDER);

        self::assertEquals(Position::DEFENDER, $defenderPosition->getValue());
    }

    public function testCreateAttacker(): void
    {
        $attackerPosition = new Position(Position::ATTACKER);

        self::assertEquals(Position::ATTACKER, $attackerPosition->getValue());
    }

    public function testCreateMidfielder(): void
    {
        $midfielderPosition = new Position(Position::MIDFIELDER);

        self::assertEquals(Position::MIDFIELDER, $midfielderPosition->getValue());
    }

    public function testCreateGoalKeeper(): void
    {
        $goalKeeperPosition = new Position(Position::GOAL_KEEPER);

        self::assertEquals(Position::GOAL_KEEPER, $goalKeeperPosition->getValue());
    }

    public function testCreateInvalidPosition(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new Position('some_other_position');
    }
}
