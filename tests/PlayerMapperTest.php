<?php

namespace UEFA\Tests;

use PHPUnit\Framework\TestCase;
use UEFA\Core\Mapper\PlayerMapper;
use UEFA\Entity\Player;

class PlayerMapperTest extends TestCase
{

    public function testObjectHydration(): void
    {
        $data = [
            [
                'name' => 'Robin van Persie',
                'position' => 'attacker',
                'quality' => 7,
                'speed' => 6
            ],
            [
                'name' => 'Robin van Persie sec',
                'position' => 'defender',
                'quality' => 10,
                'speed' => 6
            ]
        ];

        $mapper = new PlayerMapper();

        $numberOfPlayerInstances = $mapper->map($data)->filter(static function ($player){
            return $player instanceof Player;
        });

        self::assertCount(2, $numberOfPlayerInstances);
    }
}
