<?php


namespace UEFA\Core\DataSource;

use UEFA\Collection\PlayerCollection;

/**
 * Interface DataSource
 * @package UEFA\Core\DataSource
 */
interface DataSource
{

    /**
     * Connect to data source
     *
     * @return DataSource
     */
    public function collectDataFromSource(): DataSource;

    /**
     * Get all data from datasource
     *
     * @return PlayerCollection
     */
    public function getData(): PlayerCollection;
}
