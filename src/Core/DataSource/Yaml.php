<?php

namespace UEFA\Core\DataSource;

use Symfony\Component\Yaml\Yaml as SymfonyYaml;
use UEFA\Collection\PlayerCollection;
use UEFA\Core\Mapper\Mapper;

/**
 * Class Yaml
 * @package UEFA\Core\DataSource
 */
final class Yaml implements DataSource
{
    private array $data = [];

    private Mapper $mapper;

    public function __construct(Mapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @inheritDoc
     */
    public function collectDataFromSource(): Yaml
    {
        $this->data = SymfonyYaml::parseFile(__DIR__ . '/../../Data/players.yml');

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getData(): PlayerCollection
    {
        $data = $this->data['players'] ?? [];

        return $this->mapper->map($data);
    }
}
