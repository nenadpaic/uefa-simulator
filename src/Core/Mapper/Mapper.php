<?php

namespace UEFA\Core\Mapper;

use UEFA\Collection\PlayerCollection;

/**
 * Interface Mapper
 * @package UEFA\Core\Mapper
 */
interface Mapper
{
    /**
     * Transforms array of players from data source to PlayerCollection
     *
     * @param array $collection
     *
     * @return PlayerCollection
     */
    public function map(array $collection): PlayerCollection;
}
