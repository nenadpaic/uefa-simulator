<?php

namespace UEFA\Core\Mapper;

use UEFA\Collection\PlayerCollection;
use UEFA\Entity\Player;
use UEFA\ValueObject\Grade;
use UEFA\ValueObject\Position;

/**
 * Class PlayerMapper
 * @package UEFA\Core\Mapper
 */
class PlayerMapper implements Mapper
{

    /**
     * @inheritDoc
     */
    public function map(array $collection): PlayerCollection
    {
        $mappedResult = new PlayerCollection();

        foreach ($collection as $item) {
            $mappedResult->add(new Player(
                $item['name'],
                new Position($item['position']),
                new Grade($item['quality']),
                new Grade($item['speed'])
            ));
        }

        return $mappedResult;
    }
}
