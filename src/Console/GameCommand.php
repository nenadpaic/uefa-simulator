<?php

namespace UEFA\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UEFA\Entity\Player;
use UEFA\Service\Game\GameManager;

/**
 * Class GameCommand
 * @package UEFA\Console
 */
final class GameCommand extends Command
{
    protected static $defaultName = 'run:uefa-simulation';

    private GameManager $gameManger;

    public function __construct(GameManager $gameManager, string $name = null)
    {
        parent::__construct($name);

        $this->gameManger = $gameManager;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Runs UEFA game simulation')
            ->setHelp('Runs 3 games in UEFA group stage');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $selectedTeamByGameDifficulty = $this->gameManger->playLeagueGames();

            $output->writeln([
                'UEFA Games simulation',
                '========================'
            ]);

            $this->displayPlayersByPlayedGame($selectedTeamByGameDifficulty, $output);

            $this->displayInjuries($output);

        return Command::SUCCESS;

        } catch (\Exception $e) {
            $output->writeln('Error occurred: ' . $e->getMessage());

            return Command::FAILURE;
        }
    }

    /**
     * Displays injuries for each game
     *
     * @param OutputInterface $output
     *
     * @return void
     */
    private function displayInjuries(OutputInterface $output): void
    {
        $output->writeln('Injuries by game');

        foreach ($this->gameManger->injuriesReport() as $key => $injuredPlayer) {
            $game = (int) $key + 1;
            $output->writeln('Game: ' . $game);
            $output->writeln('Injured player: ' . $injuredPlayer->getName());
        }
    }

    /**
     * Displays players that played each game
     *
     * @param array $selectedTeamByGameDifficulty
     * @param OutputInterface $output
     *
     * @return void
     */
    private function displayPlayersByPlayedGame(array $selectedTeamByGameDifficulty, OutputInterface $output): void
    {
        foreach ($selectedTeamByGameDifficulty as $difficultyOfGame => $players) {
            $output->writeln('Opponent was ' . $difficultyOfGame);

            /**
             * @var Player $player;
             */
            foreach ($players as $player) {
                $output->writeln('Player name: ' . $player->getName());
                $output->writeln('Player position: ' . $player->getPosition()->getValue());
                $output->writeln('Player quality: ' . $player->getQuality()->getValue());
                $output->writeln('Player speed: ' . $player->getSpeed()->getValue());
                $output->writeln('+++++++++++++++++++++++++++');
            }

            $output->writeln('=====================================================');
        }
    }
}
