<?php

namespace UEFA\Repository;

use UEFA\Collection\GenericCollection;
use UEFA\Collection\PlayerCollection;

/**
 * Interface PlayerRepository
 * @package UEFA\Repository
 */
interface PlayerRepository
{
    /**
     * Get all players
     *
     * @return PlayerCollection
     */
    public function all(): PlayerCollection;
}
