<?php

namespace UEFA\Repository;

use UEFA\Collection\PlayerCollection;
use UEFA\Core\DataSource\DataSource;

/**
 * Class PlayerRepositoryImpl
 * @package UEFA\Repository
 */
final class PlayerRepositoryImpl implements PlayerRepository
{
    private DataSource $dataSource;

    public function __construct(DataSource $dataSource)
    {
        $this->dataSource = $dataSource->collectDataFromSource();
    }

    /**
     * @inheritDoc
     */
    public function all(): PlayerCollection
    {
        return $this->dataSource->getData();
    }
}
