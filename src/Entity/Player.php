<?php

namespace UEFA\Entity;

use UEFA\ValueObject\Grade;
use UEFA\ValueObject\Position;

/**
 * Class Player
 * @package UEFA\Entity
 */
final class Player
{
    protected Position $position;

    protected Grade $quality;

    protected Grade $speed;

    protected string $name;

    public function __construct(string $name, Position $position, Grade $quality, Grade $speed) {
        $this->quality = $quality;
        $this->speed = $speed;
        $this->name = $name;
        $this->position = $position;
    }

    public function getPosition(): Position
    {
        return $this->position;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getQuality(): Grade
    {
        return $this->quality;
    }

    public function getSpeed(): Grade
    {
        return $this->speed;
    }
}
