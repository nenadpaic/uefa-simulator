<?php

namespace UEFA\Entity;

/**
 * Class OpponentsDifficulty
 * @package UEFA\Entity
 */
final class OpponentsDifficulty
{
    public const DIFFICULTY_STRONG = 'strong';
    public const DIFFICULTY_EQUAL = 'equal';
    public const DIFFICULTY_WEAK = 'weak';
}
