<?php

namespace UEFA\Service\Game;

use UEFA\Entity\OpponentsDifficulty;
use UEFA\Service\Team\TeamManager;

/**
 * Class UefaGameManager
 * @package UEFA\Service\Game
 */
final class UefaGameManager implements GameManager
{
    private TeamManager $teamManager;

    private array $opponentsDifficultyByGame = [
        OpponentsDifficulty::DIFFICULTY_STRONG,
        OpponentsDifficulty::DIFFICULTY_EQUAL,
        OpponentsDifficulty::DIFFICULTY_WEAK
    ];

    public function __construct(TeamManager  $teamManager)
    {
        $this->teamManager = $teamManager;
    }

    /**
     * @inheritDoc
     */
    public function playLeagueGames(): array
    {
        $selectedTeamByOpponent = [];

        foreach ($this->opponentsDifficultyByGame as $opponentDifficulty) {
            $assembledTeam = $this->teamManager->selectTeamByDifficulty($opponentDifficulty);
            $selectedTeamByOpponent[$opponentDifficulty] = $assembledTeam;

            $this->teamManager->injury($assembledTeam[random_int(0, 10)]);
        }

        return $selectedTeamByOpponent;
    }

    /**
     * @inheritDoc
     */
    public function injuriesReport(): array
    {
        return $this->teamManager->getInjuredPlayers();
    }
}
