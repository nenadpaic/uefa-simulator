<?php

namespace UEFA\Service\Game;

use Exception;

/**
 * Responsible for UEFA league games managing
 *
 * Interface GameManager
 * @package UEFA\Service\Game
 */
interface GameManager
{
    /**
     * Run UEFA simulation game for defined opponents
     *
     * @return array
     * @throws Exception
     */
    public function playLeagueGames(): array;

    /**
     * Gets injuries report for each game
     *
     * @return array
     */
    public function injuriesReport(): array;
}
