<?php

namespace UEFA\Service\Team;

use UEFA\Entity\Player;
use Exception;

/**
 * Responsible for managing and assembling team
 *
 * Interface TeamManager
 * @package UEFA\Service\Team
 */
interface TeamManager
{
    /**
     * Choose team by selected difficulty
     *
     * @param string $difficulty
     * @throws Exception
     * @return array
     */
    public function selectTeamByDifficulty(string $difficulty): array;

    /**
     * Handle injury of player
     *
     * Removes player from list of available players
     *
     * @param Player $player
     */
    public function injury(Player $player): void;

    /**
     * Returns list of all injured players
     *
     * @return array
     */
    public function getInjuredPlayers(): array;
}
