<?php

namespace UEFA\Service\Team;

use UEFA\Collection\PlayerCollection;
use UEFA\Entity\OpponentsDifficulty;
use UEFA\Entity\Player;
use UEFA\Repository\PlayerRepository;
use InvalidArgumentException;
use UEFA\ValueObject\Position;
use Exception;

/**
 * Class TeamManagerImpl
 * @package UEFA\Service\Team
 */
final class TeamManagerImpl implements TeamManager
{
    private const TEAM_SIZE = 11;

    private PlayerCollection $availablePlayers;

    private array $injuries = [];

    public function __construct(PlayerRepository $playerRepository)
    {
        $this->availablePlayers = $playerRepository->all();
    }

    /**
     * @inheritDoc
     */
    public function selectTeamByDifficulty(string $difficulty): array
    {
        switch ($difficulty) {
            case OpponentsDifficulty::DIFFICULTY_STRONG:
                $selectedPlayers = $this->assembleTeam(
                    $this->availablePlayers,
                    1,
                    5,
                    4,
                    true
                );
                break;
            case OpponentsDifficulty::DIFFICULTY_EQUAL:
                $selectedPlayers = $this->assembleTeam(
                    $this->availablePlayers,
                    2,
                    4,
                    4
                );
                break;
            case OpponentsDifficulty::DIFFICULTY_WEAK;
                $selectedPlayers = $this->assembleTeam(
                    $this->availablePlayers,
                    3,
                    3,
                    4
                );
                break;
            default:
                throw new InvalidArgumentException('No formation for such difficulty');
        }

        $this->checkIsTeamReadyForGame($selectedPlayers);

        return $selectedPlayers;
    }

    /**
     * Assemble team
     *
     * @param PlayerCollection $availablePlayers
     * @param int $numberOfAttackers
     * @param int $numberOfDefenders
     * @param int $numberOfMidfielders
     * @param bool $needFastAttackers
     * @return array
     */
    private function assembleTeam(
        PlayerCollection $availablePlayers,
        int $numberOfAttackers,
        int $numberOfDefenders,
        int $numberOfMidfielders,
        bool $needFastAttackers = false
    ): array {
        $attackersSelected = $needFastAttackers ?
            $availablePlayers->sortPositionBySpeed(new Position(Position::ATTACKER)) :
            $availablePlayers->sortPositionByQuality(new Position(Position::ATTACKER));

        $defenders = $availablePlayers
            ->sortPositionByQuality(new Position(Position::DEFENDER))
            ->slice(0, $numberOfDefenders);

        $midFielders = $availablePlayers
            ->sortPositionByQuality(new Position(Position::MIDFIELDER))
            ->slice(0, $numberOfMidfielders);

        $attackers = $attackersSelected->slice(0, $numberOfAttackers);

        $goalKeeper = $availablePlayers
            ->sortPositionByQuality(new Position(Position::GOAL_KEEPER))
            ->slice(0,1);

        return array_merge($defenders, $midFielders, $attackers, $goalKeeper);
    }

    /**
     * @inheritDoc
     */
    public function injury(Player $player): void
    {
        $this->injuries[] = $player;
        $this->availablePlayers->removeElement($player);
    }

    /**
     * @inheritDoc
     */
    public function getInjuredPlayers(): array
    {
        return $this->injuries;
    }

    /**
     * Check does team has required number of players for game
     *
     * @param array $selectedTeam
     * @throws Exception
     * @return void
     */
    private function checkIsTeamReadyForGame(array $selectedTeam): void
    {
        if (count($selectedTeam) !== self::TEAM_SIZE) {
            throw new Exception(
                'To many injuries on single position, we are not able to play more games, please run simulation again'
            );
        }
    }
}
