<?php

namespace UEFA\Collection;

use UEFA\Entity\Player;
use UEFA\ValueObject\Position;
use Closure;
use Traversable;
use ArrayIterator;
use Countable;
use Iterator;

/**
 * Class PlayerCollection
 * @package UEFA\Collection
 */
final class PlayerCollection implements Countable, Iterator
{
    private array $elements;

    private int $currentIndex = 0;

    public function __construct(Player ...$elements)
    {
        $this->elements = $elements;
    }

    /**
     * @return Traversable
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->elements);
    }

    /**
     * Add new player to collection
     *
     * @param Player $element
     * @return bool
     */
    public function add(Player $element) : bool
    {
        $this->elements[] = $element;

        return true;
    }

    /**
     * Convert collection to array
     *
     * @return Player[]
     */
    public function toArray() : array
    {
        return $this->elements;
    }

    /**
     * @inheritDoc
     */
    public function current()
    {
        return $this->elements[$this->currentIndex];
    }

    /**
     * @inheritDoc
     */
    public function next(): void
    {
        $this->currentIndex++;
    }

    /**
     * @inheritDoc
     */
    public function key(): int
    {
        return $this->currentIndex;
    }

    /**
     * @inheritDoc
     */
    public function valid(): bool
    {
        return isset($this->elements[$this->currentIndex]);
    }

    /**
     * @inheritDoc
     */
    public function rewind(): void
    {
        $this->currentIndex = 0;
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count($this->elements);
    }

    /**
     * Remove element from collection by key
     *
     * @param int $key
     * @return Player|null
     */
    public function remove(int $key): ?Player
    {
        if (! isset($this->elements[$key]) && ! array_key_exists($key, $this->elements)) {
            return null;
        }

        $removed = $this->elements[$key];
        unset($this->elements[$key]);

        return $removed;
    }

    /**
     * Remove element from collection
     *
     * @param Player $element
     * @return bool
     */
    public function removeElement(Player $element) : bool
    {
        $key = array_search($element, $this->elements, true);

        if ($key === false) {
            return false;
        }

        unset($this->elements[$key]);

        return true;
    }

    /**
     * Creates new instance of collection
     *
     * @param Player ...$players
     * @return PlayerCollection
     */
    private function createFrom(Player ...$players): PlayerCollection
    {
        return new static(...$players);
    }

    /**
     * Filter collection
     *
     * @param Closure $p
     * @return PlayerCollection
     */
    public function filter(Closure $p): PlayerCollection
    {
        return $this->createFrom(...array_filter($this->elements, $p, ARRAY_FILTER_USE_BOTH));
    }

    /**
     * Find players by position
     *
     * @param Position $position
     * @return PlayerCollection
     */
    public function findByPosition(Position $position): PlayerCollection
    {
        return $this->filter(static function (Player $player) use ($position) {
            return $player->getPosition()->getValue() === $position->getValue();
        });
    }

    /**
     * Take given position and sort resulting collection by quality
     *
     * @param Position $position
     * @return PlayerCollection
     */
    public function sortPositionByQuality(Position $position): PlayerCollection
    {
        $sortedPlayers = $this->findByPosition($position)->toArray();

        usort($sortedPlayers, static function(Player $playerA, Player $playerB) {
            return ($playerA->getQuality()->getValue() < $playerB->getQuality()->getValue()) ? 1 : -1;
        });

        return new static(...$sortedPlayers);
    }

    /**
     * Take given position and sort resulting collection by speed
     *
     * @param Position $position
     * @return PlayerCollection
     */
    public function sortPositionBySpeed(Position $position): PlayerCollection
    {
        $sortedPlayers = $this->findByPosition($position)->toArray();

        usort($sortedPlayers, static function(Player $playerA, Player $playerB) {
            return ($playerA->getSpeed()->getValue() < $playerB->getSpeed()->getValue()) ? 1 : -1;
        });

        return new static(...$sortedPlayers);
    }

    /**
     * Slice
     *
     * @param int $offset
     * @param int|null $length
     * @return array
     */
    public function slice(int $offset, ?int $length = null) : array
    {
        return array_slice($this->elements, $offset, $length, true);
    }

    /**
     * Takes first element
     *
     * @return Player
     */
    public function first(): Player
    {
        return reset($this->elements);
    }
}
