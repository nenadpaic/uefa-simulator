<?php

namespace UEFA\ValueObject;

use InvalidArgumentException;

final class Position
{
    private string $position;

    public const GOAL_KEEPER = 'goal_keeper';

    public const DEFENDER = 'defender';

    public const MIDFIELDER = 'midfielder';

    public const ATTACKER = 'attacker';

    public const AVAILABLE_POSITIONS = [
      self::ATTACKER,
      self::DEFENDER,
      self::MIDFIELDER,
      self::GOAL_KEEPER
    ];

    public function __construct(string $position)
    {
        if (!in_array($position, self::AVAILABLE_POSITIONS, true)) {
            throw new InvalidArgumentException('Unknown position');
        }

        $this->position = $position;
    }

    public function getValue(): string
    {
        return $this->position;
    }
}
