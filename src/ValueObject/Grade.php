<?php

namespace UEFA\ValueObject;

use InvalidArgumentException;

final class Grade
{
    private int $grade;

    public function __construct(int $grade)
    {
        if ($grade > 10 || $grade < 1) {
            throw new InvalidArgumentException('Property must be between 1-10');
        }

        $this->grade = $grade;
    }

    public function getValue(): int {
        return $this->grade;
    }
}
